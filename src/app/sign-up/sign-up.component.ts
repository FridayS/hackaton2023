import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { API_URL } from '../constants';
import { User } from '../models/user.model';
import { UserService } from '../services/user.service';
import { CookiesService } from '../services/cookie.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent {
  email!: string;
  firstName!: string;
  lastName!: string;
  password!: string;
  error: boolean = false;

  constructor(private router: Router, private userService: UserService, private cookieService: CookiesService) { }

  onSubmit() {
    const data = {
      email: this.email,
      firstName: this.firstName,
      lastName: this.lastName,
      password: this.password
    };

    fetch(API_URL + 'users/', {
      mode: 'cors',
      method: 'POST',
      body: JSON.stringify(data)
    })
      .then(response => {
        // Si la réponse n'est pas réussie (status différent de 2xx)
        if (response.status === 401) {
          this.error = true; // Définir l'état de l'erreur à true
        } else {
          this.error = false; // Définir l'état de l'erreur à true
        }
        return response.json()
      })
      .then((data: any) => {
        if (this.error) {
          return;
        }

        // Traiter la réponse de l'API en cas de succès
        const { token, refresh_token } = data;

        // Stocker les informations de l'utilisateur dans le service
        const userData: User = {
          id: token,
          email: this.email,
        };

        this.userService.setCurrentUser(userData);

        this.cookieService.setCookieToken(token);

        // Rediriger vers la page /dashboard
        this.router.navigate(['/dashboard']);
      })
      .catch((error: any) => {
        // Gérer les erreurs de la requête ici
        console.error('Erreur de la requête:', error);
        this.error = true; // Définir l'état de l'erreur à true
      });
  }

  redirectToLogin() {
    this.router.navigate(['/sign-in']);
  }
}
