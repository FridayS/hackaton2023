import { Component } from '@angular/core';
import axios from 'axios';
import { UserService } from '../services/user.service';
import { User } from '../models/user.model';
import { Router } from '@angular/router';
import { API_URL } from '../constants';
import { CookiesService } from '../services/cookie.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent {
  email!: string;
  password!: string;
  error: boolean = false;
  errorMessage: string = '';

  constructor(private router: Router, private userService: UserService, private cookieService: CookiesService) { }

  onSubmit() {
    const data = {
      email: this.email,
      password: this.password
    };

    fetch(API_URL + 'login/', {
      mode: 'cors',
      method: 'POST',
      body: JSON.stringify(data)
    })
      .then(response => {
        // Si la réponse n'est pas réussie (status différent de 2xx)
        if (response.status === 400) {
          this.error = true; // Définir l'état de l'erreur à true
          this.errorMessage = 'Impossible de parser le json';
        } else if (response.status === 404) {
          this.error = true; // Définir l'état de l'erreur à true
          this.errorMessage = 'Utilisateur non trouvé ou mot de passe incorrecte';
        } else {
          this.error = false; // Définir l'état de l'erreur à true
        }
        return response.json()
      })
      .then((data: any) => {
        if(this.error){
          return;
        }

        // Traiter la réponse de l'API en cas de succès
        const { token, refresh_token } = data;

        // Stocker les informations de l'utilisateur dans le service
        const userData: User = {
          id: token,
          email: this.email,
        };

        this.userService.setCurrentUser(userData);

        this.cookieService.setCookieToken(token);

        // Rediriger vers la page /dashboard
        this.router.navigate(['/dashboard']);
      })
      .catch((error: any) => {
        // Gérer les erreurs de la requête ici
        console.error('Erreur de la requête:', error);
        this.error = true; // Définir l'état de l'erreur à true
      });
  }


  redirectToInscription() {
    this.router.navigate(['/sign-up']);
  }
}
