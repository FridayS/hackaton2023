import { Component } from '@angular/core';
import { Member } from '../models/member.model';

@Component({
  selector: 'app-trombinoscope',
  templateUrl: './trombinoscope.component.html',
  styleUrls: ['./trombinoscope.component.css']
})
export class TrombinoscopeComponent {
  members : Array<Member>= [];

  constructor(){
    for(let i = 0; i < 10; i++){
      let member: Member = {
        image: 'https://cours-informatique-gratuit.fr/wp-content/uploads/2017/10/avatar.png',
        firstName: 'Paul',
        lastName: 'S',
        group: 'A'
      };
      this.members.push(member)
    }
  }
}
