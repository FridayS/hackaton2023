import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { API_URL } from '../constants';
import { CookiesService } from '../services/cookie.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  error: boolean = false;
  errorMessage: string = '';
  isEditing: boolean = false;

  firstName: string = 'John';
  lastName: string = 'Doe';
  email: string = 'johndoe@example.com';
  password: string = '********';
  confirmPassword: string = '********'

  constructor(private userService: UserService, private router: Router, private cookieService: CookiesService) { }

  ngOnInit() {
    // Vérifier si aucun utilisateur n'est sauvegardé dans le service
    if (this.userService.getCurrentUser() == null) {
      // Rediriger vers la page de connexion (sign-in)
      this.router.navigate(['/sign-in']);
    }
  }

  deleteAccount() {
    //this.user.delete();
  }

  disconnect() {
    this.userService.clearCurrentUser();
    this.router.navigate(['/sign-in']);
  }

  async toggleEditMode() {
    if (this.isEditing) {
      if (this.password == this.confirmPassword) {
        await this.changeUser();
      } else {
        this.error = true;
        this.errorMessage = 'Les mots de passes ne correspondent pas';
      }
    }

    if (this.error) {
      return;
    }
    this.isEditing = !this.isEditing;
  }

  async changeUser() {
    const data = {
      email: this.email,
      firstName: this.firstName,
      lastName: this.lastName,
      password: this.password
    };

    await fetch(API_URL + 'users/', {
      mode: 'cors',
      method: 'PATCH',
      body: JSON.stringify(data),
      headers: {
        'Authorization': 'bearer ' + this.cookieService.getCookieToken()
      }
    })
      .then(response => {
        // Si la réponse n'est pas réussie (status différent de 2xx)
        if (response.status === 400) {
          this.error = true; // Définir l'état de l'erreur à true
          this.errorMessage = 'Impossible de mettre à jour l\'utilisateur';
        } else if (response.status === 403) {
          this.error = true; // Définir l'état de l'erreur à true
          this.errorMessage =   'Impossible de parser le json';
        } else if (response.status === 404) {
          this.error = true; // Définir l'état de l'erreur à true
          this.errorMessage = 'Impossible de récupérer les informations de l\'utilisateur';
        } else {
          this.error = false; // Définir l'état de l'erreur à true
        }
        return response.json()
      })
      .then((data: any) => {
        if (this.error) {
          return;
        }

        // Rediriger vers la page /dashboard
        this.router.navigate(['/profile']);
      })
      .catch((error: any) => {
        // Gérer les erreurs de la requête ici
        console.error('Erreur de la requête:', error);
        this.error = true; // Définir l'état de l'erreur à true
        this.errorMessage = 'Une erreur est survenue, veuillez réessayer';
      });
  }
}
