import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';


@Injectable({
    providedIn: 'root'
})
export class CookiesService {
    constructor(private cookieService: CookieService) { }

    getCookieToken() {
        return this.cookieService.get('tokenId');
    }

    setCookieToken(value: string) {
        this.cookieService.set('tokenId', value);
    }

    clear(){
        this.cookieService.delete('tokenId');
    }
}