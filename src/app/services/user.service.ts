import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
import { CookiesService } from './cookie.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private cookieService: CookiesService) { }

  private currentUser: User | null = null;

  getCurrentUser(): User | null {
    if (this.currentUser == null) {
      let token = this.cookieService.getCookieToken();
      if (token != null && token != '') {
        let user = {
          id: token,
          email: '',
        }
        this.currentUser = user;
      } else {
        this.currentUser == null;
      }
    }

    return this.currentUser;
  }

  setCurrentUser(user: User): void {
    this.currentUser = user;
  }

  clearCurrentUser(): void {
    this.currentUser = null;
    this.cookieService.clear();
  }
}
