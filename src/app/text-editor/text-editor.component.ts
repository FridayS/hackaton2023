import { Component } from '@angular/core';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
//import SimpleUploadAdapter from '@ckeditor/ckeditor5-upload/src/adapters/simpleuploadadapter';
import { Markdown } from '@ckeditor/ckeditor5-markdown-gfm';

@Component({
  selector: 'app-text-editor',
  templateUrl: './text-editor.component.html',
  styleUrls: ['./text-editor.component.css']
})
export class TextEditorComponent {

  public editorConfig = {
    //plugins: [Markdown],
    toolbar: ['heading', '|', 'bold', 'italic', '|', 'undo', 'redo'],
    
  };

  public editor = ClassicEditor as unknown as {
    create: any;
  };

  

  public onReady(editor: any) {
    // Logique à exécuter lorsque l'éditeur est prêt
    console.log('Editor is ready to use!', editor);
  }

  public onChange(event: any) {
    // Logique à exécuter lorsque le contenu de l'éditeur change
    console.log('Editor content changed!', event);
  }
  
}
