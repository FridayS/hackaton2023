import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(
    private router: Router,
    private userService: UserService
  ) { }

  ngOnInit() {
    // Vérifier si aucun utilisateur n'est sauvegardé dans le service
    if (this.userService.getCurrentUser() == null) {
      // Rediriger vers la page de connexion (sign-in)
      this.router.navigate(['/sign-in']);
    }
  }
}
