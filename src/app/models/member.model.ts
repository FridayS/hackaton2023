export interface Member {
    image: string,
    firstName: string,
    lastName: string,
    group: string
  }
  