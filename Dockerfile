FROM node:18-alpine
WORKDIR /app
COPY package*.json ./
COPY package*.lock ./
RUN npm i -g @angular/cli @angular-devkit/build-angular && npm i
EXPOSE 4200 49153
CMD ["npm", "start"]
